import sqlite3

connection = sqlite3.connect("data.db")

# This will do the querys
cursor = connection.cursor()

# Create the table
create_table = "CREATE TABLE users (id int, username text, password text)"
cursor.execute(create_table)

# Create a user and insert it into the db
user = (1, "daniel", "qwerty")
insert_query = "INSERT INTO users VALUES (?, ?, ?)"
cursor.execute(insert_query, user)

# Create multiple users and insert them into the db
users = [
    (2, "lol", "zxcv"),
    (3, "mymi", "lmfao")
]
cursor.executemany(insert_query, users)


select_query = "SELECT * FROM users"
for row in cursor.execute(select_query):
    print(row)

# Commit changes
connection.commit()

connection.close()
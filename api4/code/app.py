from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt import JWT, jwt_required

from security import authenticate, identity
# Flask-restful devuelve ya en formato json los diccionarios.

app = Flask(__name__)
# This need to be secret and secure
app.secret_key = "daniel"
api = Api(app)

jwt = JWT(app, authenticate, identity) # /auth

items = []

class ItemList(Resource):
    def get(self):
        return {"items" : items}, 200

class Item(Resource):
    parser = reqparse.RequestParser()
    # Only capturing price key and value
    parser.add_argument("price", type=float, required=True, help="Price missing")

    @jwt_required()
    def get(self, name):
        item = next(filter(lambda item: item["name"] == name, items), None)
        return {"item" : item}, 200 if item else 404
    
    def post(self, name):
        if next(filter(lambda item: item["name"] == name, items), None):
            return {"message" : "Item {} already exists".format(name)}, 400
        else:
            # # Fuerza que el contenido se pase a diccionario, a pesar del HEADER.
            # request_data = request.get_json(force=True)
            # # No da error si no es JSON, pero retorna None
            # request_data = request.get_json(silent=True)

            data = Item.parser.parse_args()

            new_item = {
                "name" : name,
                "price" : data["price"]
            }
            items.append(new_item)
            return new_item, 201

    def delete(self, name):
        global items
        items = list(filter(lambda x: x["name"]!=name, items))
        return {"message" : "Item deleted"}, 200

    def put(self, name):
        data = Item.parser.parse_args()

        item = next(filter(lambda item: item["name"] == name, items), None)
        if item is None:
            items.append({"name" : name, 
                            "price" : data["price"]})
        else:
            item.update(data)
        
        return item, 200

# Like @app.route()
api.add_resource(Item, "/item/<string:name>")
api.add_resource(ItemList, "/items")

app.run(debug=True, port=5000)
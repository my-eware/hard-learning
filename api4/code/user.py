import sqlite3

# This class is gonna have the hability to interact with sqlite
class User:
    def __init__(self, _id, username, password):
        self.id = _id
        self.username = username
        self.password = password

    @classmethod
    def find_by_username(cls, username):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()

        query = "SELECT * FROM users WHERE username=?"

        # Don't remove the comma, we need it to tell python is a tuple
        result = cursor.execute(query, (username,))
        # Only get one row
        row = result.fetchone()
        if row:
            #         (id, user, password)
            user = cls(*row)
        else:
            user = None

        connection.close()
        return user

    @classmethod
    def find_by_id(cls, _id):
        connection = sqlite3.connect("data.db")
        cursor = connection.cursor()

        query = "SELECT * FROM users WHERE id=?"

        # Don't remove the comma, we need it to tell python is a tuple
        result = cursor.execute(query, (_id,))
        # Only get one row
        row = result.fetchone()
        if row:
            #         (id, user, password)
            user = cls(*row)
        else:
            user = None

        connection.close()
        return user
from user import User

users = [
    User(1, "dad", "qwerty")
]

def authenticate(username, password):
    # If there isn't a username with that username returns None
    user = User.find_by_username(username)
    
    if user and user.password == password:
        return user

def identity(payload):
    user_id = payload["identity"]
    return User.find_by_id(user_id)
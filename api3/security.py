from user import User

users = [
    User(1, "dad", "qwerty")
]

# Just to find our user inmmediatly
username_mapping = {u.username: u for u in users}
userid_mapping = {u.id: u for u in users}

def authenticate(username, password):
    # If there isn't a username with that username returns None
    user = username_mapping.get(username, None)
    
    if user and user.password == password:
        return user

def identity(payload):
    user_id = payload["identity"]
    return userid_mapping.get(user_id, None)